﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest
{
    public partial class UserControlDays : UserControl
    {
        //letus create another static variable for day:
        public static string static_day;
        public UserControlDays()
        {
            InitializeComponent();
        }

        private void UserControlDays_Load(object sender, EventArgs e)
        {

        }
        public void days(int numday)
        {
            lbdays.Text = numday+"";
        }

        private void UserControlDays_click(object sender, EventArgs e)
        {
            static_day = lbdays.Text;
            choixForm choixForm = new choixForm();
            choixForm.Show();
        }
    }
}
