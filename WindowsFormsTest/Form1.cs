﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest
{
    public partial class Form1 : Form
    {
        int month, year;
        //lets create a static variable that we can pass to another fform for month and year
        public static int static_month, static_year;
        public Form1()
        {
            InitializeComponent();
        }

        

       

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            displaDays();
        }

        private void displaDays()
        {
            DateTime now = DateTime.Now;
            month = now.Month;
            year = now.Year;

            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            lbDate.Text = monthname + " " + year;

            static_month = month;
            static_year = year;
            //Lets get the first day of month
            if (month > 12)
            {
                month = 1;
                year++;
            }
            DateTime startofthemonth = new DateTime(year,month, 1);

            //get the count of days of the month 
            int days = DateTime.DaysInMonth(year,month);
            //convert the startofthe month to integer
            int dayoftheweek = Convert.ToInt32(startofthemonth.DayOfWeek.ToString("d"));

            // créons d'abord un contrôle utilisateur vide        
            for (int i =1; i< dayoftheweek;i++)
            {
                UserControl1vide ucvide = new UserControl1vide();
                daycontainer.Controls.Add(ucvide);
            }
            // on va créer  un controle de jour : user controll for days
             for(int i=1; i<=days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                daycontainer.Controls.Add(ucdays);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //clear container 
            daycontainer.Controls.Clear();
            //incrementer month to go to the next month

            month++;
            static_month = month;
            static_year = year;
            if (month > 12)
            {
                month = 1;
                year++;
            }

            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            lbDate.Text = monthname + " "+ year;

            DateTime startofthemonth = new DateTime(year,month, 1);
            //get the count of days of the month 
            int days = DateTime.DaysInMonth(year, month);
            //convert the startofthe month to integer
            int dayoftheweek = Convert.ToInt32(startofthemonth.DayOfWeek.ToString("d"));

            // créons d'abord un contrôle utilisateur vide        
            for (int i = 1; i < dayoftheweek; i++)
            {
                UserControl1vide ucvide = new UserControl1vide();
                daycontainer.Controls.Add(ucvide);
            }
            // on va créer  un controle de jour : user controll for days
            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                daycontainer.Controls.Add(ucdays);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //clear container 
            daycontainer.Controls.Clear();
            //incrementer month to return to the previous month
            month--;
            static_month = month;
            static_year = year;
            if (month > 12)
            {
                month = 1;
                year--;
            }

            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            lbDate.Text = monthname + " " + year;
            DateTime startofthemonth = new DateTime(year, month, 1);
            //get the count of days of the month 
            int days = DateTime.DaysInMonth(year, month);
            //convert the startofthe month to integer
            int dayoftheweek = Convert.ToInt32(startofthemonth.DayOfWeek.ToString("d"));

            // créons d'abord un contrôle utilisateur vide        
            for (int i = 1; i < dayoftheweek; i++)
            {
                UserControl1vide ucvide = new UserControl1vide();
                daycontainer.Controls.Add(ucvide);
            }
            // on va créer  un controle de jour : user controll for days
            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                daycontainer.Controls.Add(ucdays);
            }
        }
        

     }

        

        
 }
        
        

        

       

       
        

        

    