﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest
{
    public partial class congeForm : Form
    {
        public congeForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void congeForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = UserControlDays.static_day + "/" + Form1.static_month + "/" + Form1.static_year;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // verifier champ  non vide
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" ||
               dateTimePicker1.Value != DateTime.MinValue || dateTimePicker2.Value != DateTime.MinValue || comboBox1.SelectedIndex != -1)
            {
                MessageBox.Show("champs vide chtaaml !");
            }
            else
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=miniprojet;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");
                conn.Open();
               

                    string req = "INSERT INTO leaveRequest (textBox2.Text,textBox3.Text,dateTimePicker1.Value,dateTimePicker2.Value,textBox4.Text,comboBox1.SelectedIndex )"
                        + "VALUES (@requestid,@name,@startDate,@endDate,@statut,@leavetype)";
                SqlCommand cmd = new SqlCommand(req, conn);
                cmd.Parameters.AddWithValue("@requestid", textBox2.Text);
                cmd.Parameters.AddWithValue("@name", textBox3.Text);
                cmd.Parameters.AddWithValue("@startDate", dateTimePicker1.Value);
                cmd.Parameters.AddWithValue("@endDate", dateTimePicker2.Value);
                cmd.Parameters.AddWithValue("@statut", textBox4.Text);
                cmd.Parameters.AddWithValue("@leavetype", comboBox1.SelectedIndex);
                cmd.ExecuteNonQuery();

                

            }
        }
    }

}
        